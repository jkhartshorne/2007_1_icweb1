# README #

Project outline for 2007_1_ICWeb1

**Keywords:** Implicit Causality, Pronouns

**Overview:** Experiment 1 from Hartshorne & Snedeker (2014) Verb argument structure predicts implicit causality

------------------------------------
**Publications:** 

1. Hartshorne, Joshua K. & Jesse Snedeker. (2013).Verb argument structure predicts implicit causality: The advantages of finer-grained semantics. Language and Cognitive Processes, 28(10), 1474-1508. [link](http://joshuakhartshorne.org/papers/HartshorneSnedeker2012.pdf)

2. Hartshorne, Joshua K. & Jesse Snedeker (2009). Rapid online effects of verb argument structure on pronoun resolution. 22nd Annual Meeting of the CUNY Conference on Human Sentence Processing, Davis. [link](http://joshuakhartshorne.org/posters/CUNYICposter.pdf)

**Team:**

1. Joshua Hartshorne
2. Jesse Snedeker

**Data Collection:**

- Website. 2007-??

**Notes:**
