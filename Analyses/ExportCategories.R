stimuli <- read.csv('stimuliVerbNetted.csv')

cats.1 <- stimuli[,c(1:11)]
cats.2 <- stimuli[,c(1:10,12)]
cats.3 <- stimuli[,c(1:10,13)]
cats.4 <- stimuli[,c(1:10,14)]
cats.5 <- stimuli[,c(1:10,15)]
cats.6 <- stimuli[,c(1:10,16)]
cats.7 <- stimuli[,c(1:10,17)]
cats.8 <- stimuli[,c(1:10,18)]
cats.9 <- stimuli[,c(1:10,19)]
cats.10 <- stimuli[,c(1:10,20)]
cats.11 <- stimuli[,c(1:10,21)]
cats.12 <- stimuli[,c(1:10,22)]

colnames(cats.1)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.2)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.3)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.4)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.5)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.6)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.7)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.8)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.9)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.10)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.11)<-c(colnames(cats.1)[1:10],"category")
colnames(cats.12)<-c(colnames(cats.1)[1:10],"category")

cats.1$category<-as.character(cats.1$category)
cats.2$category<-as.character(cats.2$category)
cats.3$category<-as.character(cats.3$category)
cats.4$category<-as.character(cats.4$category)
cats.5$category<-as.character(cats.5$category)
cats.6$category<-as.character(cats.6$category)
cats.7$category<-as.character(cats.7$category)
cats.8$category<-as.character(cats.8$category)
cats.9$category<-as.character(cats.9$category)
cats.10$category<-as.character(cats.10$category)
cats.11$category<-as.character(cats.11$category)
cats.12$category<-as.character(cats.12$category)

cats<-rbind(cats.1,cats.2,cats.3,cats.4,cats.5,cats.6,cats.7,cats.8,cats.9,cats.10,cats.11,cats.12)

cats<-cats[cats$category!="",]

write.csv(levels(as.factor(cats$category)),'categories.csv')
